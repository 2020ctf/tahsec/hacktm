# HackTM CTF (2020)
HackTM CTF 2020

## Overview

**URL:** https://ctfx.hacktm.ro/home  
**Organisors:** HackTM  
**Duration:** Feb.01.10:00 EET - Feb.03.09:59 EET (48Hrs)  
**Team mates:** Team Competition  

```
Title                         Category        Points  Flag
----------------------------- --------------- ------- -------------------------------------------------------------------------
Misc                           Misc            50      HackTM{g3t_m0re_sl33p_and_dr1nk_m0re_water}
Strange PCAP                   Forensics       457     HackTM{88f1005c6b308c2713993af1218d8ad2ffaf3eb927a3f73dad3654dc1d00d4ae}
```
## Misc

* **Category:** Misc
* **Points:** 50

### Challenge

```
The dragon sleeps at night

I made this console based dragon RPG.
Go kill the beast!

nc 138.68.67.161 60004

Author: stackola

```

### Solution

The solution is using a combination of the exponential symbol and then negating an integer.

```
Welcome to our little town!
We're glad you've decided to help us fight the dragon and bring back this town to it's old glory.

We have a shop where you can buy many different weapons for your fight!
There's also a mine for you to work at. The boss is a very trusting guy, don't try to scam him please.

-------------------------------
-------------------------------
Day: 0
Time: 00:00
Your balance: $0
-------------------------------
1: Go to store
2: Go to work
3: Go to dragons cave
4: Go home
5: Storage
> 2
-------------------------------
Going to work...
Time passes...
Slowly...
-------------------------------
Boss wants to know how many hours you worked: > 9e9
9000000000.0 hours at $1/hour? That's $9000000000.0.
$9000000000.0 received.
-------------------------------
Day: 0
Time: 06:00
Your balance: $9000000000.0
-------------------------------
1: Go to store
2: Go to work
3: Go to dragons cave
4: Go home
5: Storage
> 1
-------------------------------
Welcome to the store:
-------------------------------
Level 1 Sword: 10 Damage
Price: $10
-------------------------------
Level 2 Sword: 100 Damage
Price: $100
-------------------------------
Level 3 Sword: 1,000 Damage
Price: $1,000
-------------------------------
Level 4 Sword: 10,000 Damage
Price: $1,000,000
-------------------------------
Level 5 Sword: 100,000 Damage
Price: $1,000,000,000
-------------------------------
What do you want? (1-5 or e for exit) > 5
Received Sword level 5.
-------------------------------
Day: 0
Time: 12:00
Your balance: $8000000000.0
Your sword: 5
-------------------------------
1: Go to store
2: Go to work
3: Go to dragons cave
4: Go home
5: Storage
> 5
-------------------------------
Storage for up to (1) sword.
Please note: Swords degrade by 1 level for each day they are left in storage.
-------------------------------
Storage is empty.
Do you want to deposit your sword? (y/n) > y
Deposited sword level 5
-------------------------------
Day: 0
Time: 18:00
Your balance: $8000000000.0
-------------------------------
1: Go to store
2: Go to work
3: Go to dragons cave
4: Go home
5: Storage
> 4
Your home.
Here you can take a rest.
How many days do you want to rest for? > -1
Sleeping for -1 days
A sword in storage has degraded from 5 to 6.
You woke up well rested.
-------------------------------
Day: -1
Time: 06:00
Your balance: $8000000000.0
-------------------------------
1: Go to store
2: Go to work
3: Go to dragons cave
4: Go home
5: Storage
> 3
You can not enter this cave without a sword
-------------------------------
Day: -1
Time: 12:00
Your balance: $8000000000.0
-------------------------------
1: Go to store
2: Go to work
3: Go to dragons cave
4: Go home
5: Storage
> 5
-------------------------------
Storage for up to (1) sword.
Please note: Swords degrade by 1 level for each day they are left in storage.
-------------------------------
Storage contains a sword level 6
Do you want to take the sword out? (y/n) > y
Receiving level 6 sword.
-------------------------------
Day: -1
Time: 18:00
Your balance: $8000000000.0
Your sword: 6
-------------------------------
1: Go to store
2: Go to work
3: Go to dragons cave
4: Go home
5: Storage
> 3
-------------------------------
Welcome to the dragon's cave
-------------------------------
You see the dragon sleeping next to a pile of bodies.
They look disturbingly fresh.
Carrying your level 6 sword, you walk over to the dragon
You're still dizzy from the time travelling
About half way towards the dragon, the blade starts vibrating
As if by magic, it's pulled out of your hands, and towards the dragon
As it reaches approximately Mach 2 right before impact, you take cover behind a cliff

The impact can only be compared to a small bomb. The entire cave shakes not unlike during an earthquake.
As you look up from you cover, you see the level 6 sword floating in place, just where the dragon used to be.
You walk up to the sword and inspect it closely.

On the blade you can see a faint inscription. You are pretty sure this wasn't here before:

HackTM{g3t_m0re_sl33p_and_dr1nk_m0re_water}

```

**Flag**
```
HackTM{g3t_m0re_sl33p_and_dr1nk_m0re_water}
```
---

## Strange PCAP

* **Category:** Forensics
* **Points:** 457

### Challenge

```
We managed to get all the data to incriminate our CEO for selling company secrets. Can you please help us and give us the secret data that he has leaked?

Author: Legacy 

```
[Strange.pcapng](/challengefiles/Strange.pcapng)

### Solution

We identify the type of USB device by using the vendor ID and the product ID which are announced in one of the types of USB packets.
```
$ tshark -r Strange.pcapng -T fields -e usb.bus_id -e usb.device_address -e usb.idVendor -e usb.idProduct "usb.idVendor > 0" 2>/dev/null
1       1       32903   0x00000029
1       2       10182   0x00005395
1       3       3141    0x00006723
1       15      1118    0x00000800
1       16      1423    0x00006387
```
The results are: 
32903 (0x8087) = Intel; 0x00000029
10182 (0x27c6) = Shenzhen Goodix Technology Co.,Ltd.; 0x00005395 =  Fingerprint Reader
3141 (0x0c45) = Microdia; 0x00006723 = Integrated Webcam HD
1118 (0x045e) = Microsoft Corp.; 0x00000800 = Mouse & Keyboard Detection Driver ??
1423 (0x058f) = Alcor Micro Corp.; 0x00006387 = Flash Drive


Wireshark Filter: usb.src == "1.16.1" || usb.src == "1.16.2" || usb.dst == "1.16.1" || usb.dst == "1.16.2"
No. 1224 is of interest as it has a Flag.txt in it.
Exported the USB Mass Storage part and it turned out to be a zip file with a password.
I might have to go through the keystrokes captured to get the password next...

To get the keystrokes, we just filter the usb.device_address=15 (we have identified it above) and grap the capdata.

```
tshark -r Strange.pcapng -Y 'usb.capdata && usb.device_address==15' -T fields -e usb.capdata > strangepcap_hexoutput.txt
```

For our python program to work properly, we will have to remove some invalid lines (i.e. first 4 and the last).
Then we will have to place ':' after every 2 characters.. (i.e. 0000240000000000 to 00:00:24:00:00:00:00:00).

[strangepcap_hexoutput.txt](/writeupfiles/strangepcap_hexoutput.txt)  
[usb-keyboard-parser.py](/writeupfiles/usb-keyboard-parser.py)  

Now we can run our program and use that password to unzip the file with the Flag.txt file:
```
$ python usb-keyboard-parser.py strangepcap_hexoutput.txt 
7vgj4SSL9NHVuK0D6d3F

$ unzip usb_flag_data.zip

$ cat Flag.txt 
HackTM{88f1005c6b308c2713993af1218d8ad2ffaf3eb927a3f73dad3654dc1d00d4ae}
```
**Flag**
```
HackTM{88f1005c6b308c2713993af1218d8ad2ffaf3eb927a3f73dad3654dc1d00d4ae}
```
---
